import React from 'react';

export default class DistanceSlider extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      distance: props.initial
    }
  }

  onChange(e) {
    this.props.callback(e.target.value);
    this.setState({
      distance: e.target.value
    })
  }

  render() {
    return (
      <div className="slider">
        <input type="range" min="0" max="20" step="0.1"
        value={this.state.distance} onChange={this.onChange.bind(this)}/>
        <p>速度 [ {this.state.distance} ]</p>
      </div>
    )
  }
}

import React from 'react';
import kaim from "../images/kaim.png";

import "../styles/square.sass";

export default class Square extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      width: props.libido,
      height: props.libido,
      left: 100,
      top: 100,
      angle: props.initialAngle
    }
  }

  componentDidMount() {
    this.interval = setInterval(this.move.bind(this), 10);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  move() {
    this.checkLibido();
    this.wrapPosition();
    this.normalizeAngle();
    this.applyRotation();
    this.moveForwardByVector();
  }

  checkLibido() {
    this.setState((state, props) => ({
      width: parseFloat(props.libido),
      height: parseFloat(props.libido)
    }))
  }

  wrapPosition() {
    if (this.state.left < 0) {
      this.setState(state => ({
        left: window.innerWidth - state.width
      }))
    } else if (this.state.left + this.state.width >= window.innerWidth) {
      this.setState({
        left: 0
      })
    } else if (this.state.top + this.state.width >= window.innerHeight) {
      this.setState({
        top: 0
      })
    } else if (this.state.top < 0) {
      this.setState(state => ({
        top: window.innerHeight - state.height
      }))
    }
  }

  normalizeAngle() {
    if (this.state.angle <= -360 || this.state.angle >= 360) {
      this.setState({
        angle: 0
      })
    }
  }

  applyRotation() {
    const newAngle = parseFloat(this.state.angle - -this.props.angleFactor);

    this.setState((state, props) => ({
      angle: newAngle,
      transform: `rotate(${newAngle}deg)`
    }))
  }

  moveForwardByVector() {
    const vx = Math.cos(this.state.angle * Math.PI / 180) * this.props.distance;
    const vy = Math.sin(this.state.angle * Math.PI / 180) * this.props.distance;

    this.setState(state => ({
      left: state.left + vx,
      top: state.top + vy
    }))
  }

  render() {
    return <div className="square" style={this.state}><img src={kaim}/></div>
  }
}

import React from 'react';

export default class LibidoSlider extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      libido: props.initial
    }
  }

  onChange(e) {
    this.props.callback(e.target.value);
    this.setState({
      libido: e.target.value
    })
  }

  render() {
    return (
      <div className="slider">
        <input type="range" min="50" max="500" step="1"
        value={this.state.libido} onChange={this.onChange.bind(this)}/>
        <p>リビドー [ {this.state.libido} ]</p>
      </div>
    )
  }
}

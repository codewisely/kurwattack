import React from 'react';

export default class AngleFactorSlider extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      angleFactor: props.initial
    }
  }

  onChange(e) {
    this.props.callback(e.target.value);
    this.setState({
      angleFactor: e.target.value
    })
  }

  onMouseDown(e) {
    this.props.callback(this.state.angleFactor);
  }

  onMouseUp(e) {
    this.props.callback(0);
    this.setState({
      angleFactor: 0
    })
  }

  render() {
    return (
      <div className="slider">
        <input type="range" min="-2" max="2" 
        step={this.props.step} value={this.state.angleFactor} 
        onChange={this.onChange.bind(this)} onMouseUp={this.onMouseUp.bind(this)} 
        onMouseDown={this.onMouseDown.bind(this)}/>
        <p>角度 [ {this.state.angleFactor} ]</p>
      </div>
    )
  }
}

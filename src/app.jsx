import React from 'react';
import ReactDOM from 'react-dom';

import Square from './components/Square';
import LibidoSlider from './components/LibidoSlider';
import DistanceSlider from './components/DistanceSlider';
import AngleFactorSlider from './components/AngleFactorSlider';

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      distance: 2,
      angleFactor: 0,
      libido: 200
    }
  }

  render() {
    return (
      <div>
        <div className="sliders">
          <DistanceSlider initial={2} callback={distance => this.setState({ distance })}/>
          <AngleFactorSlider initial={0} step={0.1} callback={angleFactor => this.setState({ angleFactor })}/>
          <LibidoSlider initial={200} callback={libido => this.setState({ libido })}/>
        </div>
        <Square initialAngle={0} libido={this.state.libido} distance={this.state.distance} angleFactor={this.state.angleFactor} active={this.state.active}/>
      </div>
    )
  }
}

ReactDOM.render(<App/>, document.getElementById("app"));
